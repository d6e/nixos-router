# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

# Useful commands:
# nix-collect-garbage --delete-older-than 30d
# nix-store --verify --check-contents --repair
# nix-store --optimise

{ config, pkgs, lib, ... }:

# Useful: https://github.com/disassembler/network/blob/master/portal/default.nix
let
  externalInterface = "enp1s0";
  internalInterfaces = [
    "br0"
   ];
in 
{
   boot.kernel.sysctl = {
     "net.ipv6.conf.all.forwarding" = true;
     "net.ipv6.conf.enp1s0.accept_ra" = 2;
   };
   networking = {
     enableIPv6 = true;
     useDHCP = false;
     interfaces.enp1s0.useDHCP = true;
     interfaces.br0.useDHCP = false;  # have to whitelist interfaces with DHCP enabled
     bridges.br0.interfaces = [ "enp2s0" "enp3s0" ];
     interfaces.br0.ipv4 = {
       addresses = [ {"address" = "10.10.0.1"; "prefixLength" = 24;}];
       routes = [ {"address" = "10.10.0.0"; "prefixLength" = 24; }];
     };
     nat = {
       enable = true;
       externalInterface = "${externalInterface}";
       internalInterfaces = [ "br0" ];
       internalIPs = [ "10.10.0.0/24" ];
       forwardPorts = [
         { sourcePort = 10999; destination = "10.10.0.35:10999"; proto = "udp"; }
#         { sourcePort = 3001; destination = "10.10.0.35:3001"; proto = "tcp"; }
       ];
     };
     dhcpcd = {
       persistent = true;
       extraConfig = ''
         noipv6rs
         interface ${externalInterface}
         ia_na 1
         ia_pd 2/::/60 br0/0/64
       '';
     };
     firewall = {
       enable = true;
       allowPing = true;
       allowedTCPPorts = [ 5510 5001 ];
       #interfaces.br0 = {
       #  allowedTCPPorts = [ 5510 ];
       #};
       extraCommands = let
         dropPortNoLog = port:
           ''
             ip46tables -A nixos-fw -p tcp --dport ${toString port} -j nixos-fw-refuse
             ip46tables -A nixos-fw -p udp --dport ${toString port} -j nixos-fw-refuse
           '';
         dropPortIcmpLog =
           ''
             iptables -A nixos-fw -p icmp -j LOG --log-prefix "iptables[icmp]: "
             ip6tables -A nixos-fw -p ipv6-icmp -j LOG --log-prefix "iptables[icmp-v6]: "
           '';
         refusePortOnInterface = port: interface:
           ''
             ip46tables -A nixos-fw -i ${interface} -p tcp --dport ${toString port} -j nixos-fw-log-refuse
             ip46tables -A nixos-fw -i ${interface} -p udp --dport ${toString port} -j nixos-fw-log-refuse
           '';
         acceptPortOnInterface = port: interface:
           ''
             ip46tables -A nixos-fw -i ${interface} -p tcp --dport ${toString port} -j nixos-fw-accept
             ip46tables -A nixos-fw -i ${interface} -p udp --dport ${toString port} -j nixos-fw-accept
           '';
         # IPv6 flat forwarding. For ipv4, see nat.forwardPorts
         ipv6forwardPortToHost = port: interface: proto: host:
           ''
             ip6tables -A FORWARD -i ${interface} -p ${proto} -d ${host} --dport ${toString port} -j ACCEPT
           '';
         privatelyAcceptPort = port: lib.concatMapStrings (interface: acceptPortOnInterface port interface) internalInterfaces;
         publiclyRejectPort = port: refusePortOnInterface port externalInterface;
         allowPortOnlyPrivately = port:
           ''
             ${privatelyAcceptPort port}
             ${publiclyRejectPort port}
           '';
       in lib.concatStrings [
#         (dropPortIcmpLog)
         ''
           # Set the policy for when a packet hits the end of the chain
           ip46tables --policy INPUT DROP
           ip46tables --policy FORWARD DROP
           ip46tables --policy OUTPUT ACCEPT
           # forward any ipv6 icmp traffic
           ip6tables -A INPUT -p ipv6-icmp -j ACCEPT
           ip6tables -A FORWARD -p ipv6-icmp -j ACCEPT
         ''
         (lib.concatMapStrings allowPortOnlyPrivately
           [
           ])
         (lib.concatMapStrings dropPortNoLog
           [
           ])
         ''
           # allow from trusted interfaces
           ip46tables -A FORWARD -m state --state NEW -i br0 -o enp1s0 -j ACCEPT
           # allow traffic with existing state
           ip46tables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
           # blocking forward from external interface
           ip6tables -A FORWARD -i enp1s0 -j DROP
         ''
       ];
     };
   };
   services.dhcpd4 = {
     enable = true;
     interfaces = ["br0"];
     extraConfig = ''
       subnet 10.10.0.0 netmask 255.255.255.0 {
         option domain-search "home.d6e.io";
         option subnet-mask 255.255.255.0;
         option broadcast-address 10.10.0.255;
         option routers 10.10.0.1;
         option domain-name-servers 10.10.0.1;
#         option domain-name-servers 1.0.0.1,1.1.1.1;
         range 10.10.0.50 10.10.0.254;
         default-lease-time 100;  # 10 min
         max-lease-time 100;
#         max-lease-time 7200;     # two hours
       }
     '';
     machines = [
       { ethernetAddress = "10:78:d2:73:27:4b"; ipAddress = "10.10.0.5"; hostName = "storage-node-1";}
       { ethernetAddress = "ac:87:a3:39:9c:f2"; ipAddress = "10.10.0.14"; hostName = "beryllium-wired";}
       { ethernetAddress = "a4:5e:60:bb:1b:9d"; ipAddress = "10.10.0.15"; hostName = "beryllium-wifi";}
       { ethernetAddress = "e8:6a:64:b5:dd:ef"; ipAddress = "10.10.0.16"; hostName = "bismuth-wired";}
       { ethernetAddress = "ec:71:db:e1:6e:09"; ipAddress = "10.10.0.20"; hostName = "reolink-camera";}
       { ethernetAddress = "b8:27:eb:ec:af:65"; ipAddress = "10.10.0.33"; hostName = "rpi";}
       { ethernetAddress = "d0:13:fd:63:82:6a"; ipAddress = "10.10.0.34"; hostName = "nexus5x";}
       { ethernetAddress = "70:4d:7b:6f:52:39"; ipAddress = "10.10.0.35"; hostName = "danielle-gaming";}
       { ethernetAddress = "3c:28:6d:00:90:97"; ipAddress = "10.10.0.36"; hostName = "danielle-pixel3";}
       { ethernetAddress = "3c:28:6d:10:9e:4c"; ipAddress = "10.10.0.37"; hostName = "anthony-pixel3";}
       { ethernetAddress = "70:85:C2:49:34:35"; ipAddress = "10.10.0.38"; hostName = "tungsten";}
       { ethernetAddress = "b8:27:eb:2d:09:3b"; ipAddress = "10.10.0.39"; hostName = "weather-pi";}
       { ethernetAddress = "dc:a6:32:77:bd:08"; ipAddress = "10.10.0.40"; hostName = "homeassistant";}
       { ethernetAddress = "00:50:b6:28:94:14"; ipAddress = "10.10.0.41"; hostName = "amazon-basic-usb-dongle";}
       { ethernetAddress = "b8:27:eb:d6:8c:00"; ipAddress = "10.10.0.42"; hostName = "octopi";}
       { ethernetAddress = "6e:fc:9c:10:d1:85"; ipAddress = "10.10.0.43"; hostName = "rockpro64";}
       { ethernetAddress = "f0:08:d1:d1:76:58"; ipAddress = "10.10.0.44"; hostName = "bedroom-prom-node";}
       { ethernetAddress = "02:0f:b5:c5:17:2f"; ipAddress = "10.10.0.45"; hostName = "shelly-garage-door";}
       { ethernetAddress = "d0:50:99:d0:48:21"; ipAddress = "10.10.0.100"; hostName = "freenas";}
       { ethernetAddress = "d0:50:99:e2:40:c4"; ipAddress = "10.10.0.101"; hostName = "freenas-control";}
     ];
   };
   services.radvd = {
     enable = true;
     config = ''
       interface br0
       {
         AdvSendAdvert on;
         prefix ::/64
         {
           AdvOnLink on;
           AdvAutonomous on;
         };
       };
     '';
   };
   services.avahi = {
     enable = true;
     interfaces = ["br0"];
     nssmdns = true;
     publish = {
       enable = true;
       userServices = true;
     };
   };
#   services.dnscrypt-proxy = {
#     enable = true;
#     localAddress = "127.0.0.1";
#     localPort = 43;
#   };
   networking.firewall.interfaces.br0.allowedTCPPorts = [53];
   networking.firewall.interfaces.br0.allowedUDPPorts = [53];
   services.unbound = {
     enable = true;
     allowedAccess = ["127.0.0.0/24" "10.10.0.0/24"];
     interfaces = [ "10.10.0.1" "127.0.0.1" "::1" ];
     extraConfig = ''
       forward-zone:
         name: "."
         forward-addr: 1.1.1.1@853                   
         forward-addr: 1.0.0.1@853                             
         forward-addr: 2606:4700:4700::1111@853
         forward-addr: 2606:4700:4700::1001@853
         forward-ssl-upstream: yes   

#       private-domain: "home.d6e.io";
#       local-zone: "home.d6e.io" static;
#       local-data: "aqi-sensor-1.home.d6e.io IN A 10.10.0.44";
     '';
#     forwardAddresses = [ "127.0.0.1@43" ];
   };
}
