{ config, lib, pkgs, ... }:

let
  port = 51820;
in
  {
    environment.systemPackages = with pkgs; [ wireguard ];
  
    # enable NAT
    networking.nat.enable = true;
#    networking.nat.externalInterface = "eth0";
    networking.nat.internalInterfaces = [ "wg0" ];
    networking.firewall.allowedUDPPorts = [ port ];
  
    networking.wireguard.interfaces = {
        # Enable forwarding within network
#	preSetup = "iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o br0 -j MASQUERADE";
#	postShutdown = ["iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o br0 -j MASQUERADE"];

      # "wg0" is the network interface name. You can name the interface arbitrarily.
      wg0 = {
        # Determines the IP address and subnet of the server's end of the tunnel interface.
        ips = [ "10.110.0.1/24" "fd00:d6e:1::1/64" ];
  
        # The port that Wireguard listens to. Must be accessible by the client.
        listenPort = port;
  
        # Path to the private key file.
        #
        # Note: The private key can also be included inline via the privateKey option,
        # but this makes the private key world-readable; thus, using privateKeyFile is
        # recommended.
        privateKeyFile = "~/wireguard/server.prv";
  
        peers = [
          { # Danielle's pixel 3
            publicKey = "iWyMFYKgJsBuJf/Lffi8PQMbMoOfiodd2xZNrRdBJig=";
            allowedIPs = [ "10.110.0.3/32" "fd00:d6e:1::3/128" ];
          }
          { # bismuth
            publicKey = "TOkEOufDem2UP4NciLFE5K8+KGP/z4hoS+6S+m1qokA=";
            allowedIPs = [ "10.110.0.4/32" "fd00:d6e:1::4/128" ];
          }
        ];
      };
    };
  }
