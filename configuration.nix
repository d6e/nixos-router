# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

# Useful commands:
# nix-collect-garbage --delete-older-than 30d
# nix-store --verify --check-contents --repair
# nix-store --optimise

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./router.nix
#      ./wireguard.nix
      ./node-exporter.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.kernelParams = [ "console=ttyS0,115200n8" ];
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  
  nixpkgs.config.allowBroken = false; 

  networking.hostName = "nixos-apu"; # Define your hostname.
  networking.wireless.enable = false;  # Enables wireless support via wpa_supplicant.
  documentation.nixos.enable = false;
  

  # Select internationalisation properties.
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";
  
  programs.mosh.enable = true;

  # $ nix-env -qaP | grep wget
   environment.systemPackages = with pkgs; [
     wget 
     vim
     bridge-utils
     parted
     tmux
     htop
     lsof
     tcpdump
     nmap
     git
     vulnix
     mosh
     bind
   ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.bash.enableCompletion = true;
  programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  services.journald.extraConfig = ''
    SystemMaxUse=100M
    MaxFileSec=5day
  '';
  services.openssh = {
    enable = true;
    ports = [5510];
    permitRootLogin = "prohibit-password";
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
    listenAddresses = [{"addr" = "10.10.0.1"; "port"=5510;}];
  };
  boot.tmpOnTmpfs = true; # Whether to mount a tmpfs on /tmp during boot.
  services.fstrim.enable = true;

#  system.autoUpgrade.enable = true;  # peridocially run `nix-rebuild switch --upgrade`
  nix.gc.automatic = true;
  nix.gc.dates = "weekly";
  nix.gc.options = "--delete-older-than 30d";

  security.sudo = {
	enable = true;
	wheelNeedsPassword = true;
  };

  users.users.root = {
     openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH8JZiol5YfZRH/Fw60P54tWtN3+TIGiBmxTneQOB/6/ d6e@bismuth
"
     ];
  };
  users.users.d6e = {
     isNormalUser = true;
     uid = 1000;
     openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH8JZiol5YfZRH/Fw60P54tWtN3+TIGiBmxTneQOB/6/ d6e@bismuth
"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFjsD1FUgeBPJRiM94+lZ0XFIjUlAGtN8vSKrzTeVwzU d6e@nitrogen"
    ];

     extraGroups = [ "wheel" ];
   };

  # Tell the Nix evaluator to garbage collect more aggressively.
  #   # This is desirable in memory-constrained environments that don't
  #     # (yet) have swap set up.
      environment.variables.GC_INITIAL_HEAP_SIZE = "1M";

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?

}
