# Nixos on a router
## Build a custom ISO
In my case, I had to build my own Nixos iso because I needed to install over serial. Here's what I did.
```
git clone https://github.com/NixOS/nixpkgs.git
cd nixpkgs/nixos
# Edit accordingly. (In my case I added the line `boot.kernelParams = [ "console=ttyS0,115200n8" ];` to modules/installer/cd-dvd/installation-cd-base.nix
nix-build -A config.system.build.isoImage -I nixos-config=modules/installer/cd-dvd/installation-cd-minimal.nix default.nix
```

## Install:
1. Go through the nixos install steps described in the [manual](https://nixos.org/nixos/manual/index.html#sec-installation).
2. Copy the `configuration.nix` into `/etc/nixos` and edit to preference.
3. `nixos-rebuild switch` and you're done! Hurray!
