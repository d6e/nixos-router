#!/usr/bin/env bash
rsync -az --delete ./* "root@router:/etc/nixos/" && ssh root@router "$@"
