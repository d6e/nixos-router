#!/bin/bash

if [ ! -z "$1" ]; then
  SWAPFILENAME="$1"
else
  SWAPFILENAME=swapfile1
fi
SWAPPATH="/$SWAPFILENAME" 

if [ ! -f "$SWAPPATH" ]; then
  dd if=/dev/zero of="$SWAPPATH" bs=1024 count=2048576
  chown root:root "$SWAPPATH"
  chmod 0600 "$SWAPPATH"
  mkswap "$SWAPPATH"
fi
swapon "$SWAPPATH"

