{ config, lib, pkgs, ... }:
{
#  networking.firewall.allowedTCPPorts = [ 9100 ];

  services.prometheus.exporters.node = {
    enable = true;
    openFirewall = true;
    listenAddress = "10.10.0.1";
    enabledCollectors = [
      "cpu"
      "conntrack"
      "diskstats"
      "entropy"
      "filefd"
      "filesystem"
      "hwmon"
      "pressure"
      "loadavg"
      "mdadm"
      "meminfo"
      "netdev"
      "netstat"
      "stat"
      "time"
      "vmstat"
      "systemd"
      "logind"
      "interrupts"
      "ksmd"
    ];
  };
}

